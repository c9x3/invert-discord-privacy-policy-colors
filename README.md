# [C9x3] Invert Discord Privacy Policy Colors

Tries to invert the following sites:

https://discord.com/privacy

https://discord.com/blog/*

https://support.discord.com/*

# Warning and disclaimer:
The webpage might flash white when loading.
Not all of the webpage may be inverted.

# Licensing and donation information:

https://c9x3.neocities.org/

# How to run this Userscript

- Get a userscript runner. For examlpe, Violentmonkey. 
- Create a new script and copy and paste the contents into the new userscript! 
- Save that script, exit and test the script out. 
