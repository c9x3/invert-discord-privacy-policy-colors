// ==UserScript==
// @name             [C9x3] Invert Discord Privacy Policy Colors
// @version          0.6
// @description      Tries to invert Discord Privacy Policy colors, Discord Blog colors and Discord Support colors to give you a dark theme.
// @match            https://discord.com/privacy
// @match            https://discord.com/blog/*
// @match            https://support.discord.com/*
// @downloadURL      https://gitlab.com/c9x3/invert-discord-privacy-policy-colors/-/raw/main/invert.js
// @updateURL        https://gitlab.com/c9x3/invert-discord-privacy-policy-colors/-/raw/main/invert.js
// ProjectHomepage   https://c9x3.neocities.org/
// @grant            none
// ==/UserScript==

document.querySelector('html').style.filter = 'invert(100%)'
